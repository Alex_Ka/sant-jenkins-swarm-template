#!/usr/bin/env groovy
@Library('jenkins-sant-library') _

node {

    def date = new Date();
    //для передачи данных через все шаги
    def runId = date.format('yy-MM-dd_HH-mm');
    def profileId = null;
    def profileJSONBody = null;

    def dockerAddressProp
    def confluenceCredentialsProp
    def confluenceBasePageIdProp
    def confluenceHostProp
    def lokiLoginPass
    def lokiAddress
    def loaderIdProp = "1"
    def jmeterBaseImageProp
    def gitTokenProp
    def chUrl
    def chLogin
    def chPassword
    def confluenceSpaceProp

    withCredentials([dockerCert(credentialsId: 'DockerCerts', variable: 'DOCKER_CERT_PATH'),
                     string(credentialsId: 'DockerURL', variable: 'DOCKER_URL'),
                     string(credentialsId: 'ConfluenceURL', variable: 'CONF_URL'),
                     string(credentialsId: 'ConfluenceCreds', variable: 'CONF_CR'),
                     string(credentialsId: 'ConfluenceBaseID', variable: 'CONF_ID'),
                     string(credentialsId: 'ConfluenceSpace', variable: 'CONF_SP'),
                     string(credentialsId: 'lokiURL', variable: 'LOKI_URL'),
                     string(credentialsId: 'lokiAuth', variable: 'LOKI_AUTH'),
                     string(credentialsId: 'JmeterBaseImage', variable: 'JMETER_BASE'),
                     string(credentialsId: 'GitlabTokenString', variable: 'GIT_TOKEN'),
                     string(credentialsId: 'ClickHouseURL', variable: 'CH_URL'),
                     string(credentialsId: 'ClickHouseLogin', variable: 'CH_LOGIN'),
                     string(credentialsId: 'ClickHousePassword', variable: 'CH_PASS')
    ])
            {
                System.setProperty("docker.cert.path", "$DOCKER_CERT_PATH")
                dockerAddressProp = "$DOCKER_URL"
                confluenceCredentialsProp = "$CONF_CR"
                confluenceBasePageIdProp = "$CONF_ID"
                confluenceHostProp = "$CONF_URL"
                confluenceSpaceProp = "$CONF_SP"
                lokiLoginPass = "$LOKI_AUTH"
                lokiAddress = "$LOKI_URL"
                jmeterBaseImageProp = "$JMETER_BASE"
                gitTokenProp = "$GIT_TOKEN"
                chUrl = "$CH_URL"
                chLogin = "$CH_LOGIN"
                chPassword = "$CH_PASS"

                stage("Выбор профиля") {
                    profilesList = get_profiles_list {
                        confluenceCredentials = confluenceCredentialsProp
                        confluenceBasePageId = confluenceBasePageIdProp
                        confluenceHost = confluenceHostProp
                    }
                    script {
                        env.ProfileName = input message: 'Выберите профиль', ok: 'Далее', abort: 'Отмена',
                                parameters: [choice(name: 'Имя профиля', choices: profilesList,
                                        description: 'В этом окне требуется выбрать профиль для которого требуется публикация')]
                    }
                    echo "${env.ProfileName}"

                }
                stage("Комментарий") {
                    script {
                        try {
                            timeout(time: 30, unit: 'SECONDS') { // change to a convenient timeout for you
                                env.Commentary = input(
                                        id: 'userInput', message: 'Введите, пожалуйста, комментарий', ok: 'Далее', abort: 'Отмена', parameters: [
                                        [$class: 'TextParameterDefinition', defaultValue: 'Нет комментариев', description: 'Комментарий будет добавлен к отчету', name: 'Текст комментария']
                                ])
                            }
                        } catch (err) { // timeout reached
                            env.Commentary = 'Нет комментариев'
                        }
                        echo("Комментарий: " + env.Commentary)
                    }
                    wrap([$class: 'BuildUser']) {
                        env.UserName = "${BUILD_USER}"
                        echo "${env.UserName}"
                    }
                }
                stage('Получаем ID профиля') {
                    profileId = get_profile_id {
                        confluenceCredentials = confluenceCredentialsProp
                        confluenceBasePageId = confluenceBasePageIdProp
                        confluenceHost = confluenceHostProp
                    }
                    println(profileId);
                }
                stage('Получаем тело профиля') {
                    profileJSONBody = get_profile_body_by_id_and_parse {
                        confluenceCredentials = confluenceCredentialsProp
                        confluenceBasePageId = confluenceBasePageIdProp
                        confluenceHost = confluenceHostProp
                        searchedProfileId = profileId
                    }
                    println(profileJSONBody);

                }
                stage('Удаляем ранее запущенное') {
                    clean_before_start {
                        dockerAddress = dockerAddressProp
                        loaderId = loaderIdProp
                        processProfileJSON = profileJSONBody
                    }
                }
                stage('Создаем набор микросервисов') {
                    create {
                        clickHouseUrl = chUrl
                        clickHouseLogin = chLogin
                        clickHousePassword = chPassword
                        lokiAuth = lokiLoginPass
                        lokiUrl = lokiAddress
                        confluenceCredentials = confluenceCredentialsProp
                        confluenceBasePageId = confluenceBasePageIdProp
                        confluenceHost = confluenceHostProp
                        jmeterBaseImage = jmeterBaseImageProp
                        dockerAddress = dockerAddressProp
                        loaderId = loaderIdProp
                        gitToken = gitTokenProp
                        runIdForServices = runId
                        processProfileJSON = profileJSONBody
                    }
                }
            }

    stage('Ожидаем пока выполнятся') {
        wait_loading {
            processProfileJSON = profileJSONBody
        }
    }
    withCredentials([dockerCert(credentialsId: 'DockerCerts', variable: 'DOCKER_CERT_PATH'),
                     string(credentialsId: 'DockerURL', variable: 'DOCKER_URL'),
                     string(credentialsId: 'ConfluenceURL', variable: 'CONF_URL'),
                     string(credentialsId: 'ConfluenceCreds', variable: 'CONF_CR'),
                     string(credentialsId: 'ConfluenceBaseID', variable: 'CONF_ID'),
                     string(credentialsId: 'ConfluenceSpace', variable: 'CONF_SP'),
                     string(credentialsId: 'lokiURL', variable: 'LOKI_URL'),
                     string(credentialsId: 'lokiAuth', variable: 'LOKI_AUTH'),
                     string(credentialsId: 'JmeterBaseImage', variable: 'JMETER_BASE'),
                     string(credentialsId: 'GitlabTokenString', variable: 'GIT_TOKEN'),
                     string(credentialsId: 'ClickHouseURL', variable: 'CH_URL'),
                     string(credentialsId: 'ClickHouseLogin', variable: 'CH_LOGIN'),
                     string(credentialsId: 'ClickHousePassword', variable: 'CH_PASS')
    ])
            {
                System.setProperty("docker.cert.path", "$DOCKER_CERT_PATH")
                dockerAddressProp = "$DOCKER_URL"
                confluenceCredentialsProp = "$CONF_CR"
                confluenceBasePageIdProp = "$CONF_ID"
                confluenceHostProp = "$CONF_URL"
                confluenceSpaceProp = "$CONF_SP"
                lokiLoginPass = "$LOKI_AUTH"
                lokiAddress = "$LOKI_URL"
                jmeterBaseImageProp = "$JMETER_BASE"
                gitTokenProp = "$GIT_TOKEN"
                chUrl = "$CH_URL"
                chLogin = "$CH_LOGIN"
                chPassword = "$CH_PASS"

                stage('Удаляем микросервисы') {
                    clean_after_loading {
                        dockerAddress = dockerAddressProp
                        loaderId = loaderIdProp
                        processProfileJSON = profileJSONBody
                    }
                }
                stage('Собираем и публикуем результаты') {
                    resultHTTPCode = collect_results_and_publish {
                        clickHouseUrl = chUrl
                        clickHouseLogin = chLogin
                        clickHousePassword = chPassword
                        confluenceCredentials = confluenceCredentialsProp
                        confluenceBasePageId = confluenceBasePageIdProp
                        confluenceHost = confluenceHostProp
                        confluenceSpace = confluenceSpaceProp
                        loaderId = loaderIdProp
                        usedProfileId = profileId
                        usedRunId = runId
                        usedProfileJSON = profileJSONBody
                    }
                }
            }
}