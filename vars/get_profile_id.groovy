def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    echo "Получаем ID профиля по имени"
    def id=new SANTClass(script:this,
            confluenceCredentials:config.confluenceCredentials,
            confluenceBasePageId:config.confluenceBasePageId,
            confluenceHost:config.confluenceHost).confluenceGetProfileIdByName(env.ProfileName);
    echo "Найден успешно"
    currentBuild.result = 'SUCCESS' //FAILURE to fail
    return id;
}