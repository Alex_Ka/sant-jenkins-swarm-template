def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Останавливаем запущенное"
    new SANTClass(script:this,
            dockerAddress:config.dockerAddress,
            loaderId:config.loaderId).dockerCleanServices(config.processProfileJSON)
    echo "Выполнено успешно"
    currentBuild.result = 'SUCCESS' //FAILURE to fail
    return this;
}