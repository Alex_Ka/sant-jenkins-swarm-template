def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Получаем тело профиля по ID"
    def bodyHTML=new SANTClass(script:this,
            confluenceCredentials:config.confluenceCredentials,
            confluenceBasePageId:config.confluenceBasePageId,
            confluenceHost:config.confluenceHost).confluenceGetProfileBodyById(config.searchedProfileId);
    echo "Найден успешно"
    echo "Разбираем тело профиля"
    def bodyJSON=new SANTClass(script:this).confluenceParseProfileBody(bodyHTML);
    echo "Разобрано успешно"

    currentBuild.result = 'SUCCESS' //FAILURE to fail
    return bodyJSON;
}