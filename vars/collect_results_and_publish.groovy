def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    def Script= new SANTClass()
    Script.setScript(this)

    echo "Ожидаем(иногда пакетные отправки не полностью обрабатываются до сбора статистики)"
    Script.jenkinsWaitBuffer()
    echo "Завершено ожидание"

    childName="Result_"+config.usedRunId

    Script.setClickHouseUrl(config.clickHouseUrl)
    Script.setClickHouseLogin(config.clickHouseLogin)
    Script.setClickHousePassword(config.clickHousePassword)

    Script.setConfluenceCredentials(config.confluenceCredentials)
    Script.setConfluenceBasePageId(config.confluenceBasePageId)
    Script.setConfluenceHost(config.confluenceHost)
    Script.setConfluenceSpace(config.confluenceSpace)

    echo "Получаем статистику"
    def resultJSONBody=Script.clickHouseCollectResults(env.ProfileName,config.usedRunId,config.usedProfileJSON);
    echo resultJSONBody
    echo "Получена успешно"
    echo "Собираем тело дочерней страницы"
    def childHTMLBody=Script.confluenceBuildChildPageBody(resultJSONBody,env.Commentary,env.UserName)
    echo childHTMLBody
    echo "Собрано успешно"
    echo "Публикуем дочернюю страницу"
    def resultCode=Script.confluencePublishChildPage(config.usedProfileId,childName, childHTMLBody)

    if (resultCode.toString().equals("200")) currentBuild.result = 'SUCCESS' else currentBuild.result = 'FAILURE' //FAILURE to fail
    return resultCode;
}